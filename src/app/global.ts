import { Country } from './country'

export interface Global {

    /**
     * Interface used as a model to retrieve the worldwide summary data from the API
     */

    Global: {
        NewConfirmed: any;
        TotalConfirmed: number;
        NewDeaths: number;
        TotalDeaths: number;
        NewRecovered: number;
        TotalRecovered: number;
    }
    Countries: Country[];
}
