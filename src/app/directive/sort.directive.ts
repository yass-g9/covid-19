import { Directive, ElementRef, HostListener, Input, Renderer2 } from '@angular/core';
import {Sort} from '../util/sort';

@Directive({
  selector: '[appSort]'
})
export class SortDirective {

  /**
   * Class used to sort the different component that are stored in the countries table of the worldwide page.
   * Model used : https://medium.com/nerd-for-tech/how-to-sort-table-rows-according-column-in-angular-9-b04fdafb4140
   */

  @Input() appSort: Array<any>;

  constructor(private renderer: Renderer2, private targetElem: ElementRef) {}

  @HostListener("click")
  sortData() {
    const sort = new Sort();
    const elem = this.targetElem.nativeElement;
    const order = elem.getAttribute("data-order");
    const property = elem.getAttribute("data-name");

    let down = ["down0", "down1", "down2", "down3", "down4", "down5", "down6"];
    let up = ["up0", "up1", "up2", "up3", "up4", "up5", "up6"];

    let id = elem.getAttribute("id");
    let number = parseInt(id.slice(-1));

    this.appSort.sort(sort.startSort(property, order));

    if (id.substring(0,1) === "u") {
      elem.setAttribute("class", "fa fa-chevron-circle-up");
      up.splice(number, 1);
      up.forEach(val => {
        let element = document.getElementById(val);
        element.setAttribute("class", "fa fa-angle-up");
      });
      down.forEach(val => {
        let element = document.getElementById(val);
        element.setAttribute("class", "fa fa-angle-down");
      });
    }
    else {
      elem.setAttribute("class", "fa fa-chevron-circle-down");
      down.splice(number, 1);
      down.forEach(val => {
        let element = document.getElementById(val);
        element.setAttribute("class", "fa fa-angle-down");
      });
      up.forEach(val => {
        let element = document.getElementById(val);
        element.setAttribute("class", "fa fa-angle-up");
      });
    }


    
  }

}
