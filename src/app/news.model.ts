import { User } from './user.model';

export class News {
    
    /**
    * Class used to create a piece of news.
    */
   
    description: string;
    country: string;
    user: User;
    date: string;

    constructor(des: string, country: string, user: User, date: string) {
        this.description = des;
        this.country = country;
        this.user = user;
        this.date = date;
    }
}