export class CountryDayOne {

    /**
     * Class used to store the daily data of a country
     */

    newConfirmed: number[];
    totalConfirmed: number[];
    newDeaths: number[];
    totalDeaths: number[];
    newRecovered: number[];
    totalRecovered: number[];
    active: number[];
    date: string;

    constructor(newConfirmed: number[], totalConfirmed: number[], newDeaths: number[], totalDeaths: number[], newRecovered: number[], totalRecovered: number[], active: number[]) {
        let monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
        "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
        this.newConfirmed = newConfirmed;
        this.totalConfirmed = totalConfirmed;
        this.active = active;
        this.newDeaths = newDeaths;
        this.totalDeaths = totalDeaths;
        this.newRecovered = newRecovered;
        this.totalRecovered = totalRecovered;
        let date = new Date();
        this.date = date.getDate().toString() + " " + monthNames[date.getMonth()] + " " + date.getFullYear().toString();
    }
w
}