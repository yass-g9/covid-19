export interface Country {

    /**
     * Interface used as a model to retrieve the summary data of a country from the API
     */

    Country: string;
    NewConfirmed: number;
    TotalConfirmed: number;
    NewDeaths: number;
    TotalDeaths: number;
    NewRecovered: number;
    TotalRecovered: number;
    Date: Date;
}
