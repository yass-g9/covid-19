export class Country {

    /**
     * Class used to store the data of the country and to calculate the number of active cases as well as
     * the ratio of mortality and recovered.
     */

    country: string;
    newConfirmed: number;
    totalConfirmed: number;
    activeCases: number;
    newDeaths: number;
    totalDeaths: number;
    mortalityRate: number;
    newRecovered: number;
    totalRecovered: number;
    recoveryRate: number;
    date: Date;

    constructor(name: string, newConfirmed: number, totalConfirmed: number, newDeaths: number, totalDeaths: number, newRecovered: number, totalRecovered: number, date: Date) {
        this.country = name;
        this.newConfirmed = newConfirmed;
        this.totalConfirmed = totalConfirmed;
        this.activeCases = totalConfirmed - totalRecovered;
        this.newDeaths = newDeaths;
        this.totalDeaths = totalDeaths;
        this.newRecovered = newRecovered;
        this.totalRecovered = totalRecovered;
        this.date = date;
        this.recoveryRate = Math.round(this.totalRecovered/this.totalConfirmed*10000)/100;
        this.mortalityRate = Math.round(this.totalDeaths/this.totalConfirmed*10000)/100;
    }
}
