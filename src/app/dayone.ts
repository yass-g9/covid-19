export interface Dayone {

    /**
     * Interface as a model to retireve the daily data from the API
     */

    Country: string;
    Province: string;
    Confirmed: number;
    Deaths: number;
    Recovered: number;
    Active: number;
    Date: Date;
}


