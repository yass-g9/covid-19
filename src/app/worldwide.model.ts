export class Worldwide {

    /**
     * Class used to store the worlwide summary data.
     */

    newConfirmed: number;
    totalConfirmed: number;
    activeCases: number;
    newDeaths: number;
    totalDeaths: number;
    mortalityRate: number;
    newRecovered: number;
    totalRecovered: number;
    recoveryRate: number;

    constructor(newConfirmed: number, totalConfirmed: number, newDeaths: number, totalDeaths: number, newRecovered: number, totalRecovered: number) {
        this.newConfirmed = newConfirmed;
        this.totalConfirmed = totalConfirmed;
        this.activeCases = totalConfirmed - totalRecovered;
        this.newDeaths = newDeaths;
        this.totalDeaths = totalDeaths;
        this.newRecovered = newRecovered;
        this.totalRecovered = totalRecovered;
        this.recoveryRate = Math.round(this.totalRecovered/this.totalConfirmed*10000)/100;
        this.mortalityRate = Math.round(this.totalDeaths/this.totalConfirmed*10000)/100;
    }
}
