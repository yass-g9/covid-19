export class User {

    /**
    * Class used to create a user by his UID, his displayed name and his email.
    */
   
    uid: string;
    displayName: string;
    email: string;

    constructor(uid: string, dn: string, email: string) {
        this.uid = uid;
        this.displayName = dn;
        this.email = email;
    }
}