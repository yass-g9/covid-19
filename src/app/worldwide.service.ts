import { HttpClient } from '@angular/common/http';
import { Injectable, OnInit } from '@angular/core';
import firebase from "firebase/app";
import { AngularFireAuth } from '@angular/fire/auth';
import { throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { User } from './user.model';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class WorldwideService {

  private summaryUrl = 'https://api.covid19api.com/summary';

  private dayOneUrl = 'https://api.covid19api.com/dayone/country/'

  private worldLiveUrl = 'https://api.covid19api.com/world'

  private user: User;

  private authorizedUsersEmail: string[] = []

  constructor(private http: HttpClient, private afAuth: AngularFireAuth, private firestore: AngularFirestore) {
    // Retrieve the users allowed to add pieces of news from the Google firestore
    this.firestore.collection("users").doc("authorized").get().subscribe(doc => {
      this.authorizedUsersEmail = doc.data()["email"].split(",");
    })
  }

  public sendSummaryGetRequest() {
    /**
     * Collects the worldwide summary data.
     */
    return this.http.get(this.summaryUrl).pipe(
      map((data:any) => {
        return data;
      }), catchError( error => {
        return throwError( 'Something went wrong!' );
      })
    );
  }

  public sendDayOneGetRequest(country: string) {
    /**
     * Collects the day by day data for a specified country.
     */
    return this.http.get(this.dayOneUrl + country);
  }

  public sendWorldLiveGetRequest() {
    /**
     * Collects the worldwide day by day data.
     */
    return this.http.get(this.worldLiveUrl).pipe(
      map((data:any) => {
        return data;
      }), catchError( error => {
        return throwError( 'Something went wrong!' );
      })
    );
  }

  async signInWithGoogle() {
    /**
     * Method used to sign in with a Google account usong the authentication service of the Google 
     * firebase.
     */
    const credentials = await this.afAuth.signInWithPopup(new firebase.auth.GoogleAuthProvider());
    this.user = {
      uid: credentials.user.uid,
      displayName: credentials.user.displayName,
      email: credentials.user.email
    };
    localStorage.setItem("user", JSON.stringify(this.user));
    this.updateUserData();
  }

  private updateUserData() {
    /**
     * Adds a user to the firestore.
     */
    this.firestore.collection("users").doc(this.user.uid).set({
      uid: this.user.uid,
      displayName: this.user.displayName,
      email: this.user.email
    }, { merge: true });
  }

  userSignedIn(): boolean {
    /**
     * Method used to know if a user has signed in or not.
     */
    return JSON.parse(localStorage.getItem("user")) != null;
  }

  getUser() {
    /**
     * Method used to get the pieces of information of the user who is connected.
     */
    if (this.user == null && this.userSignedIn()){
      this.user = JSON.parse(localStorage.getItem("user"));
    }
    return this.user;
  }

  authorizedUser(): boolean {
    /**
     * Returns True if the user is in the list of authorized users that are allowed to write 
     * new pieces of news.
     */
    for (let i = 0; i < this.authorizedUsersEmail.length; i++){
      if (this.authorizedUsersEmail[i] === this.user.email){
        return true;
      }
    }
  }
  
  signOut(){
    /**
     * Method used to disconnect a user from the website.
     */
    this.afAuth.signOut();
    localStorage.removeItem("user");
    this.user = null;
  }

}
