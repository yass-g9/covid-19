import { Component, OnInit } from '@angular/core';
import { Global } from '../global';
import { Worldwide } from '../worldwide.model';
import { WorldwideService } from '../worldwide.service';
import { AngularFirestore } from '@angular/fire/firestore';
import { ChartType, ChartOptions, ChartDataSets } from 'chart.js';
import { SingleDataSet, Label, monkeyPatchChartJsLegend, monkeyPatchChartJsTooltip } from 'ng2-charts';
import { Country } from '../country.model';
import { Router, RouterLinkWithHref } from '@angular/router';
import { Dayone } from '../dayone';
import { World } from '../world';
import { MatDialog, MatDialogConfig } from "@angular/material/dialog";
import { DialogComponent } from '../dialog/dialog.component';
import { User } from '../user.model';
import { News } from '../news.model';
import { CountryDayOne } from '../countryDayOne.model';

@Component({
  selector: 'app-worldwide',
  templateUrl: './worldwide.component.html',
  styleUrls: ['./worldwide.component.css']
})
export class WorldwideComponent implements OnInit {

  // Variable used for the summary data table
  worldwide: Worldwide;
  
  // Variable used for the summary data of countries table
  countries: Country[];

  // Variable used for the daily data of a country when clicking on it in the table
  countryPerDay: Country[];

  // Variable used to store the pieces of news
  news: News[] = [];
  worldwideNews: News[] = [];

  // Variable used to know how many pieces of news are stored in the Google firestore
  size: number = 0;

  monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
  "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

  //Variables needed for worldwide's pie chart
  public pieChartOptions: ChartOptions = {
    responsive: true,
    title: {
      display: true
    }
  };
  public pieChartLabels: Label[] = [['Dead Cases'], ['Recovered Cases'], ['Active Cases']];
  public pieChartData; SingleDataSet; 
  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartPlugins = [];

  // Variables needed for worldwide's bar chart
  barChartOptions: ChartOptions = {
    responsive: true,
    title: {
      display: true
    }
  };
  barChartLabels: Label[];
  barChartType: ChartType = 'bar';
  barChartLegend = true;
  barChartPlugins = [];
  barChartData: ChartDataSets[] = [];

  // Variables needed for worldwide's line chart
  lineChartData: ChartDataSets[] = [];
  lineChartLabels: Label[] = [];
  lineChartOptions: ChartOptions = {
    responsive: true,
    title: {
      display: true
    }
  };
  lineChartLegend = true;
  lineChartPlugins = [];
  lineChartType = 'line';

  constructor(public worldwideService: WorldwideService, private firestore: AngularFirestore, private router: Router, private dialog: MatDialog) {
    monkeyPatchChartJsTooltip();
    monkeyPatchChartJsLegend(); 

    // Retrieve the number of news that are stored in the Google firestore
    this.firestore.collection("news").get().subscribe(snap => {
      this.size = snap.size;
      this.showNews(this.size); 
    })
  }

  ngOnInit(): void {
    // Going to the top of the page
    window.scrollTo(0, 0);

    // Updating the different values by calling the API
    this.update();

    // Retrieving the data updating that is shown in the tables
    this.countries = this.getCountriesData();

    // Update of the daily cases and total cases charts 
    this.updateCharts();

    // Showing the connect or disconnect button whether the user is disconnected or not
    if (!this.worldwideService.userSignedIn()) {
      this.showSignIn();
    }
    else {
      this.showSignOut();
    }
  }

  private update(): void {
    // Call of the API to get the data of the worldwide summary and the country by country summary
    this.worldwideService.sendSummaryGetRequest().subscribe((data: Global) => {
      this.updateSummary(data);
      this.updateCountries(data);
    })
  }

  private updateSummary(global: Global) {
    /**
     * Method used to update the worldwide summary data of the first table of the page.
     */
    let worldwide: Worldwide;

    // Storing the data in a Worldwide variable that will calculate the number of active cases and the ratio with the values in input
    worldwide = new Worldwide(global.Global.NewConfirmed, 
      global.Global.TotalConfirmed,
      global.Global.NewDeaths,
      global.Global.TotalDeaths,
      global.Global.NewRecovered,
      global.Global.TotalRecovered);

    // Storing the variable in the local storage
    localStorage.setItem("summary", JSON.stringify(worldwide));
    this.worldwide = worldwide;

    // Updating the data of the worldwide pie chart
    this.pieChartData = [this.worldwide.totalDeaths, this.worldwide.totalRecovered, this.worldwide.activeCases];
  }

  private updateCountries(global: Global) {
    /**
     * Method used to update the summary data of each country that is stored in the firestore.
     */
    let countries: Country[] = [];
    let len = global.Countries.length;

    for (let i = 0; i < len - 1; i++) {
      // Creating a variable with the summary data of a country which will be stored in the local storage in order to have access to it when going to the country page
      let c = new Country(global.Countries[i].Country, global.Countries[i].NewConfirmed, global.Countries[i].TotalConfirmed, global.Countries[i].NewDeaths, global.Countries[i].TotalDeaths, global.Countries[i].NewRecovered, global.Countries[i].TotalRecovered, global.Countries[i].Date);
     
      // Storing the summary data of each country in the Google firestore
      this.firestore.collection("summary").doc(global.Countries[i].Country).set({
        name: c.country,
        newCases: c.newConfirmed,
        newDeaths: c.newDeaths,
        newRecovered: c.newRecovered,
        totalCases: c.totalConfirmed,
        totalDeaths: c.totalDeaths,
        totalRecovered: c.totalRecovered,
        mortalityRate: c.mortalityRate,
        recoveryRate: c.recoveryRate,
        active: c.activeCases
      })
      countries.push(c);
    }
    localStorage.setItem("countries", JSON.stringify(countries));
  }

  private async updateDayOneData(country: Country) {
    /**
     * Method used to collect the daily data of one country since the beginning of the pandemic there.
     */

    this.countryPerDay = [];
    let name: string = country.country.toLowerCase().split(" ").join("-");

    // Creating lists for each data in order to have the different daily values 
    let newConfirmed: number = 0;
    let newDeaths: number = 0;
    let newRecovered: number = 0;
    let tabNewConfirmed: number[] = [];
    let tabTotalConfirmed: number[] = [];
    let tabNewDeaths: number[] = [];
    let tabTotalDeaths: number[] = [];
    let tabNewRecovered: number[] = [];
    let tabTotalRecovered: number[] = [];
    let tabActive: number[] = [];

    // Call of the API to retrieve the daily data for one country since the beginning of the pandemic
    await (this.worldwideService.sendDayOneGetRequest(name)).subscribe((data: Dayone[]) => {
      data.forEach((day: Dayone) => {
        if (day.Province == "") {
          // The data retrieved is the total number of confirmed, dead and recovered cases
          // In order to have the daily data of new cases, I had to do substraction
          newConfirmed = day.Confirmed - newConfirmed;
          tabNewConfirmed.push(newConfirmed);
          newDeaths = day.Deaths - newDeaths;
          tabNewDeaths.push(newDeaths);
          newRecovered = day.Recovered - newRecovered;
          tabNewRecovered.push(newRecovered);
          tabActive.push(day.Active);
          tabTotalDeaths.push(day.Deaths);
          tabTotalConfirmed.push(day.Confirmed);
          tabTotalRecovered.push(day.Recovered);  
          newConfirmed = day.Confirmed;
          newDeaths = day.Deaths;
          newRecovered = day.Recovered;
        }
      })

      // Creating a variable in order to send the collected data to the country page
      let countryDayOne = new CountryDayOne(tabNewConfirmed, tabTotalConfirmed, tabNewDeaths, tabTotalDeaths, tabNewRecovered, tabTotalRecovered, tabActive);
      let size = this.size;

      // Going to the page of the country which has been selected in the table. The summary and daily data is sent also to the country page
      this.router.navigate(['/country', name], {state: {data: {country, countryDayOne, size}}});
    });
  }

  private updateCharts() {
    /**
     * Method used to update the different charts with the data collected and updated before.
     */

    // Cretaing variables in order to only stored the data of the seven last days
    let dataBarDeaths: number[] = [];  
    let dataBarRecovered: number[] = []; 
    let dataBarConfirmed: number[] = [];
    let labelBar = [];

    // Creating variable in order to stored the number of cases since the beginning of the pandemic
    let dataLineDeaths: number[] = [];  
    let dataLineRecovered: number[] = []; 
    let dataLineConfirmed: number[] = [];
    let labelLine = [];

    // Call of the API in order to retrieve the daily data since the beginning of the pandemic
    this.worldwideService.sendWorldLiveGetRequest().subscribe((data: World[]) => {
      let len = data.length;
      let dateBar = new Date();
      dateBar.setDate(dateBar.getDate()+1);
      let dateLine = new Date();
      dateLine.setDate(dateLine.getDate()+1);

      // Getting the data of the last seven days 
      for (let i = 1; i < 8; i++) {
        dateBar.setDate(dateBar.getDate()-1);
        let dateString: string = dateBar.getDate().toString() + " " + this.monthNames[dateBar.getMonth()];
        labelBar.unshift(dateString);
        dataBarDeaths.unshift(data[len-i].NewDeaths); 
        dataBarRecovered.unshift(data[len-i].NewRecovered);
        dataBarConfirmed.unshift(data[len-i].NewConfirmed);
      }

      // Getting the data since the beginning of the pandemic
      for (let j = 0; j < len - 1; j++) {
        dateLine.setDate(dateLine.getDate()-1);
        let dateString: string = dateLine.getDate().toString() + " " + this.monthNames[dateLine.getMonth()];
        labelLine.unshift(dateString);
        dataLineDeaths.push(data[j].NewDeaths); 
        dataLineRecovered.push(data[j].NewRecovered);
        dataLineConfirmed.push(data[j].NewConfirmed);
      }
    });

    // Updating the data in the charts
    this.barChartLabels = labelBar;
    this.barChartData = [
      {data: dataBarDeaths, label: "Daily Deaths"},
      {data: dataBarRecovered, label: "Daily Recovered"},
      {data: dataBarConfirmed, label: "Daily New Cases"}
    ];

    this.lineChartLabels = labelLine;
    this.lineChartData = [
      {data: dataLineDeaths, label: "Total Deaths"},
      {data: dataLineRecovered, label: "Total Recovered"},
      {data: dataLineConfirmed, label: "Total New Cases"}
    ];
  }


  async goToCountryComponent(country: Country) {
    /**
     * Opens the component used for a specific country, putting in parameter the  summary and the day by day 
     * data of the country selected.
     */
    await this.updateDayOneData(country);
  }

  private getSummaryData(): Worldwide {
    /**
     * Return the worldwide summary data stored locally.
     */
    return JSON.parse(localStorage.getItem("summary"));
  }

  private getCountriesData(): Country[] {
    /**
     * Returns the summary data of each country
     */
    return JSON.parse(localStorage.getItem("countries"));
  }

  async signIn() {
    /**
     * Method used to open a dialog component to connect to the website through Google's account.
     */
    await this.worldwideService.signInWithGoogle();
    if (this.worldwideService.userSignedIn()) {
      this.showSignOut();
    }
  }

  showSignIn() {
    /**
     * Method used in order to appear the button needed for the user
     * to connect to the website.
    */
    let buttonIn = document.getElementById("signIn");
    buttonIn.style.display = "block";
    let buttonOut = document.getElementById("signOut");
    buttonOut.style.display = "none";
    let buttonNews = document.getElementById("addNews");
    buttonNews.style.display = "none";
  }

  signOut() {
    /**
     * Method used to disconnect the user from the website.
    */
    this.worldwideService.signOut();
    if (!this.worldwideService.userSignedIn()) {
      this.showSignIn();
    }
  }

  showSignOut() {
    /**
     * Method used in order to appear the buttons needed for the user to disconnect himself from 
     * the website and to add a news only if the user is authorized.
    */
    let buttonIn = document.getElementById("signIn");
    buttonIn.style.display = "none";
    let buttonOut = document.getElementById("signOut");
    buttonOut.style.display = "block";
    if (this.worldwideService.authorizedUser()) {
      let buttonNews = document.getElementById("addNews");
      buttonNews.style.display = "block";
    } else {
      let buttonNews = document.getElementById("addNews");
      buttonNews.style.display = "none";
    }
  }

  addNews() {
    /**
     * Method used in order to appear the button needed for the user
     * to connect to the website.
    */
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.hasBackdrop = true;
    dialogConfig.position = {
      top: '100px',
      left: '100px',
    };
    dialogConfig.height = "400px";
    dialogConfig.width = "300px";

    // Open the dialog to add a piece of news
    const dialogRef = this.dialog.open(DialogComponent, dialogConfig);

    // Once the dialof is closed
    dialogRef.afterClosed().subscribe(
      data => {
        // Only if the user has chosen to save the piece of news he wrote
        if (data != undefined) {
          let user: User = this.worldwideService.getUser();
          let newDate = new Date();

          // We add the information of the news in the Google firestore 
          this.firestore.collection("news").doc((this.size+1).toString()).set({
            uid: user.uid,
            email: user.email,
            displayName: user.displayName,
            description: data['description'],
            country: data['country'],
            date: newDate.getDate().toString() + " " + this.monthNames[newDate.getMonth()] + " " + newDate.getFullYear().toString()
          })
          this.size = this.size + 1;
          this.showNews(this.size);
        }
      }
    );  
  }

  async showNews(size: number) {
    /**
     * Method used to add a piece of news in the firestore data through a dialog component that appears.
     */
    this.worldwideNews = [];
    for (let i = 1; i < size + 1; i++) {
      // We retireve the pieces of news that are stored in the Google firestore
      await this.firestore.collection("news").doc(i.toString()).get().subscribe(doc => {
        let user = new User(doc.data()['uid'], doc.data()['displayName'], doc.data()['email']);
        this.worldwideNews.push(new News(doc.data()['description'], doc.data()['country'], user, doc.data()['date']));
      })
    }
  }

}
