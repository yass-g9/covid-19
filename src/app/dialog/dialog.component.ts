import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from "@angular/material/dialog";

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css']
})

export class DialogComponent implements OnInit {

  /**
   * The different part of the dialog is created in the HTML file where I used the component of @angular/material.
   * Here, I create a form in order to get the values back when the user clicks on the button save.
   */

  form: FormGroup;
  countries = JSON.parse(localStorage.getItem("countries"));

  constructor(private fb: FormBuilder, private dialogRef: MatDialogRef<DialogComponent>) {
    // Creation of the form group
    this.form = this.fb.group({
      'description': ["", Validators.required],
      'country': ["", Validators.required]
    });
  }

  ngOnInit() {
    
  }

  save() {
    this.dialogRef.close(this.form.value);
  }

  close() {
    this.dialogRef.close();
  }

}
