export interface World {

    /**
     * Interface used when retrieving the data from the worldwide summary API.
     */

    NewConfirmed: number;
    TotalConfirmed: number;
    NewDeaths: number;
    TotalDeaths: number;
    NewRecovered: number;
    TotalRecovered: number;
}
