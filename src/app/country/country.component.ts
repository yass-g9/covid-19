import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { Country } from '../country.model';
import { ChartType, ChartOptions, ChartDataSets } from 'chart.js';
import { SingleDataSet, Label, monkeyPatchChartJsLegend, monkeyPatchChartJsTooltip, BaseChartDirective } from 'ng2-charts';
import { WorldwideService } from '../worldwide.service';
import { AngularFirestore } from '@angular/fire/firestore';
import { User } from '../user.model';
import { News } from '../news.model';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import { DialogComponent } from '../dialog/dialog.component';
import { CountryDayOne } from '../countryDayOne.model';


@Component({
  selector: 'app-country',
  templateUrl: './country.component.html',
  styleUrls: ['./country.component.css']
})
export class CountryComponent implements OnInit {

  // Variable used to store the summary data of the country
  country: Country;

  // Variable used to store the daily data of the country
  countryDayOne: CountryDayOne;

  // Variable sued to store the pieces of news only that are related to this country
  countryNews: News[] = [];

  // Variable used to know how many pieces of news are stored in the Google firestore
  size: number;

  monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
  "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

  // Variables needed for country's pie chart
  public pieChartOptions: ChartOptions = {
    responsive: true,
  };;
  public pieChartLabels: Label[] = [['Dead Cases'], ['Recovered Cases'], ['Active Cases']];
  public pieChartData: SingleDataSet;
  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartPlugins = [];

  // Variables needed for country's bar chart
  barChartOptions: ChartOptions = {
    responsive: true,
  };
  barChartLabels: Label[];
  barChartType: ChartType = 'bar';
  barChartLegend = true;
  barChartPlugins = [];
  barChartData: ChartDataSets[] = [];

  // Variables needed for country's line chart
  lineChartData: ChartDataSets[] = [];
  lineChartLabels: Label[] = [];
  lineChartOptions = {
    responsive: true
  };
  lineChartLegend = true;
  lineChartPlugins = [];
  lineChartType = 'line';

  constructor(public worldwideService: WorldwideService, private firestore: AngularFirestore, private dialog: MatDialog) { 
    // Updating the data that we are receiving from the worldwide page
    this.country = history.state.data.country;
    this.size = history.state.data.size;
    this.countryDayOne = history.state.data.countryDayOne;

    monkeyPatchChartJsTooltip();
    monkeyPatchChartJsLegend();
  }

  ngOnInit(): void {
    // Going to the top of the page
    window.scrollTo(0, 0);

    // Show the pieces of news related to the country
    this.showNews(this.size);

    // Updating the data of the pie chart data of the country
    this.pieChartData = [this.country.totalDeaths, this.country.totalRecovered, this.country.activeCases];

    this.updateCharts();

    // Showing the connect or disconnect button whether the user is disconnected or not
    if (!this.worldwideService.userSignedIn()) {
      this.showSignIn();
    }
    else {
      this.showSignOut();
    }
  }

  private updateCharts() {
    /**
     * Method used to update the graph when the country's page is opened. It will take the different 
     * data of the country since the first day the pandemic started there.
     */

    // Cretaing variables in order to only stored the data of the seven last days
    let dataNewDeaths: number[] = [];  
    let dataNewRecovered: number[] = []; 
    let dataNewConfirmed: number[] = [];
    let label = [];

    // Creating variable in order to stored the number of cases since the beginning of the pandemic
    let dataTotalDeaths: number[] = [];  
    let dataTotalRecovered: number[] = []; 
    let dataTotalConfirmed: number[] = [];
    let labelLine = [];

    let date = new Date();
    let dateString = date.getDate().toString() + " " + this.monthNames[date.getMonth()] + " " + date.getFullYear().toString();

    let dateLine = new Date();
    let dateLineString = dateLine.getDate().toString() + " " + this.monthNames[dateLine.getMonth()] + " " + dateLine.getFullYear().toString();

    let len = this.countryDayOne.active.length;
    if (dateString != this.countryDayOne.date) {
      date.setDate(date.getDate()-1);
      dateString = date.getDate().toString() + " " + this.monthNames[date.getMonth()] + " " + date.getFullYear().toString();
      dateLine.setDate(dateLine.getDate()-1);
      dateLineString = dateLine.getDate().toString() + " " + this.monthNames[dateLine.getMonth()] + " " + dateLine.getFullYear().toString();
    }
    for (let i = len - 8; i < len - 1; i++) {
      label.unshift(dateString);
      dataNewDeaths.push(this.countryDayOne.newDeaths[i]); 
      dataNewRecovered.push(this.countryDayOne.newRecovered[i]);
      dataNewConfirmed.push(this.countryDayOne.newConfirmed[i]);
      date.setDate(date.getDate()-1);
      dateString = date.getDate().toString() + " " + this.monthNames[date.getMonth()] + " " + date.getFullYear().toString();
    }
    for (let i = 0; i < len - 1; i++) {
      labelLine.unshift(dateLineString);
      dataTotalDeaths.push(this.countryDayOne.totalDeaths[i]); 
      dataTotalRecovered.push(this.countryDayOne.totalRecovered[i]);
      dataTotalConfirmed.push(this.countryDayOne.totalConfirmed[i]);
      dateLine.setDate(dateLine.getDate()-1);
      dateLineString = dateLine.getDate().toString() + " " + this.monthNames[dateLine.getMonth()] + " " + dateLine.getFullYear().toString();
    }

    // Updating the bar and line charts with the retrieved data
    this.barChartLabels = label;
    this.barChartData = [
      {data: dataNewDeaths, label: "Daily Deaths"},
      {data: dataNewRecovered, label: "Daily Recovered"},
      {data: dataNewConfirmed, label: "Daily New Cases"}
    ];

    this.lineChartLabels = labelLine;
    this.lineChartData = [
      {data: dataTotalDeaths, label: "Total Deaths"},
      {data: dataTotalRecovered, label: "Total Recovered"},
      {data: dataTotalConfirmed, label: "Total Cases"}
    ];
  }

  async signIn() {
    /**
     * Method used to open a dialog component to connect to the website through Google's account.
     */
    await this.worldwideService.signInWithGoogle();
    if (this.worldwideService.userSignedIn()) {
      this.showSignOut()
    }
  }

  showSignOut() {
    /**
     * Method used in order to appear the buttons needed for the user to disconnect himself from 
     * the website and to add a news only if the user is authorized.
    */
    let buttonIn = document.getElementById("signIn1");
    buttonIn.style.display = "none";
    let buttonOut = document.getElementById("signOut1");
    buttonOut.style.display = "block";
    if (this.worldwideService.authorizedUser()) {
      let buttonNews = document.getElementById("addNews1");
      buttonNews.style.display = "block";
    } else {
      let buttonNews = document.getElementById("addNews1");
      buttonNews.style.display = "none";
    }
  }

  signOut() {
    /**
     * Method used to disconnect the user from the website.
    */
    this.worldwideService.signOut();
    if (!this.worldwideService.userSignedIn()) {
      this.showSignIn();
    }
  }

  showSignIn() {
    /**
     * Method used in order to appear the button needed for the user
     * to connect to the website.
    */
    let buttonIn = document.getElementById("signIn1");
    buttonIn.style.display = "block";
    let buttonOut = document.getElementById("signOut1");
    buttonOut.style.display = "none";
    let buttonNews = document.getElementById("addNews1");
    buttonNews.style.display = "none";
  }

  addNews() {
    /**
     * Method used to add a piece of news in the firestore data through a dialog component that appears.
     */
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;
    dialogConfig.hasBackdrop = true;
    dialogConfig.position = {
      top: '100px',
      left: '100px',
    };
    dialogConfig.height = "400px";
    dialogConfig.width = "300px";

    // Open the dialog to add a piece of news
    const dialogRef = this.dialog.open(DialogComponent, dialogConfig);

    // Once the dialof is closed
    dialogRef.afterClosed().subscribe(
      data => {
        // Only if the user has chosen to save the piece of news he wrote
        if (data != undefined) {
          let user: User = this.worldwideService.getUser();
          let newDate = new Date();
          
          // We add the information of the news in the Google firestore 
          this.firestore.collection("news").doc((this.size+1).toString()).set({
            uid: user.uid,
            email: user.email,
            displayName: user.displayName,
            description: data['description'],
            country: data['country'],
            date: newDate.getDate().toString() + " " + this.monthNames[newDate.getMonth()] + " " + newDate.getFullYear().toString()
          })
          this.size = this.size + 1;
          this.showNews(this.size);
        }
      }
    );  
  }

  async showNews(size: number) {
    /**
     * Method used to store the pieces of news in order to show them in a table at the end of 
     * the page.
     */
    this.countryNews = [];
    for (let i = 1; i < size + 1; i++) {
      // We retireve the pieces of news that are stored in the Google firestore
      await this.firestore.collection("news").doc(i.toString()).get().subscribe(doc => {
        if (doc.data()['country'] === this.country.country) {
          let user = new User(doc.data()['uid'], doc.data()['displayName'], doc.data()['email']);
          this.countryNews.push(new News(doc.data()['description'], doc.data()['country'], user, doc.data()['date']));
        }
      })
    }
  }

  
}
