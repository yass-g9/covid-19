// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyB8A9J4eHtx2RFkEfi_xoZxsj6TkruPPog",
    authDomain: "covid19-c2cf1.firebaseapp.com",
    databaseURL: "https://covid19-c2cf1.firebaseio.com",
    projectId: "covid19-c2cf1",
    storageBucket: "covid19-c2cf1.appspot.com",
    messagingSenderId: "222251290593",
    appId: "1:222251290593:web:2150f6b87a6e58cb014ca0",
    measurementId: "G-1FLHCWHPT1"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
